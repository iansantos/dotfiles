��    
      l      �       �      �                  !   $     F     L     j  4   y  �  �     V     n     �     �  )   �     �  %   �     �  J              
                       	           %d Connected %d Connected All Devices Devices Help No available or connected devices Reply Turn on to connect to devices Type a message Valent must be installed to connect and sync devices Project-Id-Version: gnome-shell-extension-valent
Report-Msgid-Bugs-To: https://github.com/andyholmes/gnome-shell-extension-valent/issues
PO-Revision-Date: 2023-11-10 02:30+0100
Last-Translator: Gianni Lerro <glerro@pm.me>
Language-Team: Italian <>
Language: it
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1)
X-Generator: Gtranslator 42.0
 %d Connesso %d Connessi Tutti i dispositivi Dispositivi Aiuto Nessun dispositivo disponibile o connesso Rispondi Attiva per connetterti ai dispositivi Scrivi un messaggio Valent deve essere installato per connettere e sincronizzare i dispositivi 