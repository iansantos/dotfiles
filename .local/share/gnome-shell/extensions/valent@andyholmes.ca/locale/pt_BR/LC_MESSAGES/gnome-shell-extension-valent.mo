��    
      l      �       �      �                  !   $     F     L     j  4   y  �  �     �     �     �     �  +   �  	   �           '  G   ;           
                       	           %d Connected %d Connected All Devices Devices Help No available or connected devices Reply Turn on to connect to devices Type a message Valent must be installed to connect and sync devices Project-Id-Version: gnome-shell-extension-valent
Report-Msgid-Bugs-To: https://github.com/andyholmes/gnome-shell-extension-valent/issues
PO-Revision-Date: 2023-11-02 21:44-0300
Last-Translator: Victor L. Pavan <victorpavan001@gmail.com>
Language-Team: Brazilian Portuguese <victorpavan001@gmail.com>
Language: pt_BR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1)
X-Generator: Gtranslator 45.3
 %d Conectado %d Conectados Todos os Dispositivos Dispositivos Ajuda Nenhum dispositivo disponível ou conectado Responder Ative para conectar dispositivos Digite uma mensagem Valent precisa estar instalado para conectar e sincronizar dispositivos 