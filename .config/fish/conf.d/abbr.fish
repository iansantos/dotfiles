### ADB ###
abbr -a ab 'adb pull /dev/block/dm-0'
abbr -a ac 'adb connect'
abbr -a ad 'doas adb devices'
abbr -a ak 'adb kill-server'
abbr -a al 'adb logcat -v color -b all "*:*"'
abbr -a ap 'adb pair'
abbr -a apl 'adb pull'
abbr -a aps 'adb push'
abbr -a as 'adb shell'
abbr -a at 'adb shell pm trim-caches 100G'

### RPM-OSTREE ###
abbr -a rc 'rpm-ostree cleanup -bpmr'
abbr -a rd 'rpm-ostree db diff'
abbr -a rka --set-cursor 'rpm-ostree kargs --append=%'
abbr -a rke 'rpm-ostree kargs --editor'
abbr -a rre --set-cursor 'rpm-ostree rebase fedora:fedora/%/x86_64/silverblue'
abbr -a rro 'rpm-ostree rollback'
abbr -a rs 'rpm-ostree status --advisories --verbose'
abbr -a ru 'rpm-ostree upgrade'

### OSTREE ###
abbr -a op 'doas ostree admin pin 0'
abbr -a orr 'ostree remote refs fedora'
abbr -a opu 'doas ostree admin pin -u'

### FLATPAK ###
abbr -a f 'flatpak'
abbr -a fc 'flatpak uninstall --unused -y ; flatpak uninstall --delete-data -y'
abbr -a fh 'flatpak history'
abbr -a fi 'flatpak install'
abbr -a fim 'flatpak info --show-metadata'
abbr -a fin 'flatpak info'
abbr -a fl 'flatpak list'
abbr -a fla 'flatpak list --columns application --app'
abbr -a flg 'flatpak list --columns application | rg -S'
abbr -a flr 'flatpak list --runtime'
abbr -a fm 'flatpak --user mask'
abbr -a fr 'flatpak run'
abbr -a frc 'flatpak run --command=sh'
abbr -a fs 'flatpak search'
abbr -a fu 'flatpak update -y'
abbr -a fum 'flatpak --user mask --remove'
abbr -a fun 'flatpak uninstall -y'

### GIT ###
abbr -a g 'git'
abbr -a ga 'git add'
abbr -a gaa 'git add -A'
abbr -a gap 'git_add_all_commit_amend_no_message_push_force'
abbr -a gb 'git branch'
abbr -a gc --set-cursor 'git commit -m "%"'
abbr -a gca 'git commit --amend --no-edit'
abbr -a gcm --set-cursor 'git commit -am "%"'
abbr -a gcl 'git clone --depth 1'
abbr -a gco 'git checkout'
abbr -a gd 'git diff'
abbr -a gdd 'git diff --stat | tail -n 1'
abbr -a gds 'git diff --staged'
abbr -a gf 'git fetch'
abbr -a gh 'git update-index --assume-unchanged'
abbr -a ghl 'git ls-files -v | rg "^h"'
abbr -a gl 'git log --oneline'
abbr -a gla 'git log --oneline --patch'
abbr -a glc 'git log --oneline --patch origin/HEAD..HEAD'
abbr -a gp 'git push'
abbr -a gpf 'git push -f'
abbr -a gpl 'git pull'
abbr -a gplp 'fd -I -t d --exact-depth 2 --search-path ~/Desktop/Projects -x fish -c "echo ; git -C {} pull --depth 1 -v"'
abbr -a gr 'git restore'
abbr -a grs 'git restore --staged'
abbr -a gre 'git remote'
abbr -a gru 'git remote get-url origin'
abbr -a gs 'git status --untracked-files=all'
abbr -a guh 'git update-index --no-assume-unchanged'

### PODMAN ###
abbr -a p 'podman'
abbr -a pd 'podman run -it --rm --rmi archlinux'
abbr -a pgs 'podman pod ps | awk "NR>=2 {print \$2}" | while read -l line ; podman generate systemd --new -n --files $line ; end'
abbr -a pl 'podman image list ; echo ; podman ps -a'
abbr -a pnc 'for network in caddy homer immich jellyfin libreddit nextcloud registry servarr syncthing vaultwarden ; podman network create $network ; end'
abbr -a pq 'podman search --filter is-official'
abbr -a pr 'podman rm'
abbr -a pri 'podman rmi'
abbr -a ps 'podman start'
abbr -a psp 'podman system prune --all --volumes'
abbr -a pst 'podman stop'
abbr -a pu 'podman auto-update'

### SYSTEMCTL ###
abbr -a start-pods 'systemctl --user start pod-caddy pod-homer pod-immich pod-jellyfin pod-libreddit pod-nextcloud pod-registry pod-servarr pod-syncthing pod-vaultwarden'
abbr -a stop-pods 'systemctl --user stop pod-caddy pod-homer pod-immich pod-jellyfin pod-libreddit pod-nextcloud pod-registry pod-servarr pod-syncthing pod-vaultwarden'
abbr -a sysd 'systemctl disable --now'
abbr -a sysdr 'systemctl daemon-reload'
abbr -a syse 'systemctl enable --now'
abbr -a sysr 'systemctl restart'
abbr -a syss 'systemctl start'
abbr -a sysss 'systemctl status'
abbr -a syst 'systemctl stop'
abbr -a sysud 'systemctl --user disable --now'
abbr -a sysudr 'systemctl --user daemon-reload'
abbr -a sysue 'systemctl --user enable --now'
abbr -a sysur 'systemctl --user restart'
abbr -a sysus 'systemctl --user start'
abbr -a sysuss 'systemctl --user status'
abbr -a sysut 'systemctl --user stop'

### TAILSCALE ###
abbr -a tse tailscale set --shields-up
abbr -a tsd tailscale set --shields-up=false

### TAR ###
abbr -a tarc 'tar caf'
abbr -a tarx 'tar xvf'

### JOURNALCTL ###
abbr -a j 'journalctl --reverse'
abbr -a j3 'journalctl --reverse -p 3'
abbr -a j4 'journalctl --reverse -p 4'
abbr -a jb 'journalctl --reverse -b 0'
abbr -a jf 'journalctl -f'
abbr -a jg 'journalctl --reverse | rg -S'

### FISH ###
abbr -a fp 'fish -P'
abbr -a hc 'history clear'
abbr -a hd 'history delete'
abbr -a r 'source ~/.config/fish/conf.d/abbr.fish'

### BASIC ###
abbr -a b 'bat -p'
abbr -a bl 'doas btrfs subvolume list /'
abbr -a bu 'btrfs filesystem usage / 2>/dev/null | awk "NR==8 {print \$3}"'
abbr -a c 'clear'
abbr -a chownr 'doas chown $USER:$USER -R'
abbr -a countext 'fd -u -t f | awk -F . "{print \$NF}" | sort | uniq -c'
abbr -a cp 'cp -ia'
abbr -a curl 'curl -OL'
abbr -a d 'pwd'
abbr -a df 'df -h -x tmpfs -x devtmpfs --total'
abbr -a fas --set-cursor 'du -hd 1 % | sort -hr'
abbr -a fd 'fd -u'
abbr -a free 'free -m'
abbr -a fz 'find -type f -printf "%d\t%P\n" | sort -k1n | cut -f2 | sd "^./" "" | fzf --preview "bat --color always {}" --reverse | xargs -i -o -r vi {}'
abbr -a h 'htop'
abbr -a i+ 'doas chattr -R +i'
abbr -a i- 'doas chattr -R -i'
abbr -a l. 'eza -a -1 --group-directories-first'
abbr -a ll 'eza -l --git --group-directories-first --octal-permissions'
abbr -a ls 'eza -al --git --group-directories-first --octal-permissions'
abbr -a lt 'eza -aT --group-directories-first'
abbr -a mkdir 'mkdir -pv'
abbr -a mv 'mv -i'
abbr -a ping 'ping -c 5'
abbr -a rg 'rg -Suu'
abbr -a rm 'rm -i'
abbr -a rmr 'rm -rI'
abbr -a total 'fd -u -t f | count'
abbr -a v 'vim'
abbr -a x 'exit'

### EMACS ###
abbr -a e 'emacsclient -c'

### NVIM ###
abbr -a n 'nvim'
abbr -a st 'stylua --config-path ~/.config/nvim/stylua.toml ~/.config/nvim'

### GENERAL ###
abbr -a au 'fd -u -t file updater.sh -p ~/.var/app/org.mozilla.firefox/.mozilla/firefox -x {} -bsu'
abbr -a abbrs 'abbr -l | while read -l line; test (string length $line) -gt 4; and echo $line; end | sort | uniq -u'
abbr -a cc 'dconf reset -f /org/gnome/portal/ ; tracker3 reset -rs ; rm -rf ~/.local/share/recently-used.xbel ~/.cache/tracker3 ~/.cache/thumbnails ~/.cache/rygel ~/.cache/samba ~/.var/app/org.gnome.Loupe/data/recently-used.xbel ~/.var/app/org.videolan.VLC/cache'
abbr -a myip 'curl -s https://api.protonmail.ch/vpn/location | jq -r .IP'
abbr -a ns 'nvidia-smi -l 1'
abbr -a pu 'pip list --outdated --format=columns | awk "NR>=3 {print \$1}" | xargs pip install -U'
abbr -a rpmg 'rpm -qa --qf %{NAME}-%{VERSION}\n | sort | rg -S'
abbr -a rpml 'rpm -qa --qf  %{NAME}-%{VERSION}\n | sort ; echo ; rpm -qa | count'
abbr -a rsync 'rsync -ah --no-i-r --stats --info PROGRESS2'
abbr -a subsc 'srtcleaner -Birc UTF-8 .'
abbr -a t 'toolbox enter'
abbr -a u 'rpm-ostree upgrade ; podman auto-update ; flatpak update -y ; fwupdmgr update -y --no-reboot-check ; fish_update_completions ; fisher update'
abbr -a vs 'flatpak run com.vscodium.codium --ozone-platform-hint=wayland --enable-features=WaylandWindowDecorations .'

### YT-DLP ###
abbr -a y 'yt-dlp'
abbr -a yf 'yt-dlp --downloader ffmpeg --hls-use-mpegts'
abbr -a yg --set-cursor 'yt-dlp --force-generic-extractor --external-downloader-args ffmpeg:"-v 8 -stats" "%"'
abbr -a yt 'yt-dlp --abort-on-error --abort-on-unavailable-fragment --embed-metadata --sponsorblock-remove default --write-subs --write-auto-subs --embed-subs --compat-options no-live-chat --compat-options no-keep-subs --concurrent-fragments 64 -P Videos -o "%(uploader)s/%(title)s.%(ext)s" -a -'
abbr -a ytl --set-cursor 'while sleep 5 ; yt-dlp -S res:1080 --fixup never % ; end'
abbr -a ytm 'yt-dlp -x --abort-on-error --abort-on-unavailable-fragment --embed-metadata --embed-thumbnail --concurrent-fragments 64 -P Music -o "%(uploader)s/%(playlist)s/%(title)s.%(ext)s" --ppa "ffmpeg: -c:v mjpeg -vf crop=in_h" -a -'
abbr -a ytp 'yt-dlp --abort-on-error --abort-on-unavailable-fragment --embed-metadata --sponsorblock-remove default --write-subs --write-auto-subs --embed-subs --compat-options no-live-chat --compat-options no-keep-subs --concurrent-fragments 64 -P Videos -o "%(uploader)s/%(playlist)s/%(title)s.%(ext)s" -a -'
abbr -a yts 'yt-dlp --embed-thumbnail --embed-metadata'

function git_add_all_commit_amend_no_message_push_force
    echo "git add -A && git commit --amend --no-edit && git push -f"
    read -P "Are you sure you want to proceed? (y/n) " confirmation
    if test "$confirmation" = "y"
        git add -A && git commit --amend --no-edit && git push -f
    else
        echo "Operation canceled"
    end
end
