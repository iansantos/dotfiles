if status is-interactive
    # Commands to run in interactive sessions can go here
end

set -gx PATH /var/home/$USER/.local/bin /usr/local/bin /usr/local/sbin /usr/bin /usr/sbin
set -g fish_greeting

bind \es 'fish_commandline_prepend doas'

zoxide init fish --cmd cd | source
