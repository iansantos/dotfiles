[Unit]
Description=Immich Pod Quadlet

[Pod]
Network=immich.network
PodName=immich
UserNS=keep-id

[Service]
ExecStartPre=/bin/fish -c 'mkdir -p ~/.config/containers/systemd/immich/volumes/{data,database,cache,redis,tailscale}'
Restart=on-failure
