[Unit]
Description=Vaultwarden Pod Quadlet

[Pod]
Network=vaultwarden.network
PodName=vaultwarden
UserNS=keep-id

[Service]
ExecStartPre=/bin/fish -c 'mkdir -p ~/.config/containers/systemd/vaultwarden/volumes/{data,tailscale}'
Restart=on-failure
